{
  pkgs ? import (fetchTarball https://github.com/NixOS/nixpkgs-channels/archive/b58ada326aa612ea1e2fb9a53d550999e94f1985.tar.gz) {}
}:

with pkgs;

let
  packages = rec {
    migrations_no_congestion = callPackage ./pkgs/migrations_no_congestion {simgrid = simgrid_3_28_custom;};    
    simulations_schedule_param = callPackage ./pkgs/simulations_schedule_param {simgrid = simgrid_3_28_custom;};
    simgrid_3_28_custom = callPackage ./pkgs/simgrid_3_28_custom/custom.nix {};    
inherit pkgs; # similar to `pkgs = pkgs;` This lets callers use the nixpkgs version defined in this file.

};
in
  packages
