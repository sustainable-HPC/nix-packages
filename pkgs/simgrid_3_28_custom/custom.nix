{ simgrid, fetchurl } :

simgrid.overrideAttrs(oldAttrs: rec {
  version = oldAttrs.version + "-3.28_mig";
  rev = "09fa05320ec059e3ada0d42efbb5287f69755aa2"; # The desired SimGrid commit.
  src = fetchurl {
    url = "https://gitlab.uspdigital.usp.br/sustainable-HPC/custom-simgrid-3.28/-/archive/${rev}/simgrid-${rev}.tar.gz";
    sha256 = "sha256-krHO4xk1HLQfzdsZ613grnq8lRTgUYzKo92N9/j6HeA=";
  };
})
