
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "simulations_schedule_param";
  version = "1.1";

  src = fetchgit {
    url = "https://gitlab.uspdigital.usp.br/sustainable-HPC/simgrid-cnemesis";
    rev = "17e173ee30e5a30b1057ec4b4f37992abf4ef9cf";
    sha256 = "sha256-pSUcZYMgl2ry0cPZLFiO9EW6yI/YvlsjLSHja6xmibg=";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}

