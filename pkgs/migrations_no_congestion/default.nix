
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "migrations_no_congestion";
  version = "1";

  src = fetchgit {
    url = "https://gitlab.uspdigital.usp.br/sustainable-HPC/migrations-without-congestion-c-nemesis";
    rev = "ebc1354d16ad9070d0dc6b4def7357ce0d934338";
    sha256 = "sha256-Q4pfbNFvSJJb3+GplziYKliGNpHW6z+JHUJHskLjgPU=";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}